function marketFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "In this section, you will need to identify the cancer type, stage, line and any biomarkers that you would like to include in your forecast structure. Depending on the type of cancer you select, the subsequent options (i.e. cancer subset, staging, line etc) will be tailored to your specific cancer type. Once confirmed, you will then need to define the relevant time periods for your forecast along with your preferred means of patient input type (incidence/ progressed/ relapsed). Once complete, you will have set up the main building block of your forecast – the target patient population for which your product will be/ is indicated for.";
  }
}

function trendingFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "The trending section trends out historical product data, for inline products, assuming all else remains the same. In other words, events that will occur in the future should not be included in this section but rather in the Eventing section. There are three options that the user can use to create their baseline trended forecast: VAR, JD Custom and Regression.";
  }
}

function simScoreFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "SimScore is a predictive peak share scoring system whereby each drug is given a score (from 1-10 where 1=very poor and 10=very good), by level, for different clinical, market and company attributes. These scores are then applied a weight by attribute / level to provide shares. By defining the status of the products (i.e. Inline vs new product – which may come from the trending section if used) the outputs provide two charts. The left chart compares the latest historical shares vs. the SimScore modelled shares in order to validate the SimScore. While the right chart predicts the likely peak share for new-to-market products, based on their launch dates.";
  }
}

function eventingFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "Apply any future events which will affect the current market trend through the Events section for all the products defined in the Trending section. Example of events include new product launches or share adjustment changes. If SimScore has been used previously to predict peak share, including the SimScore (under the Event Type) will automatically populate assumptions for specified events. Hint: events are shares added to the trending figures for inline products. For new products, shares are simply calculated from the date of launch to peak share.";
  }
}

function therapyFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "This section allows you to accurately model patients on treatment and patients who progress to the next line of treatment, at each time point in your model. This is important for accurately converting patients to units and revenues in the final section and is also important should you wish to model further lines of treatment (and factor in treatment restriction based on usage at previous lines). The therapy duration section also accounts for treatment holidays – an important consideration when forecasting in oncology.";
  }
}

function conversionFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "This section is the final step in your model creation – taking your post evented patients on treatment and converting these through to units and/or packs and revenue. Please note that given the complexity of the oncology market, there are 24 possible different selection combinations within the conversion pop up box. However, the important thing is to select the options that best reflect your product and cancer.";
  }
}

function consolidateFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "Consolidation allows you to combine several forecasts together e.g. for several countries or scenarios, by bringing the outputs from each forecast together into a single file. From here, the outputs from each forecast can be viewed individually or combined. The user can consolidate outputs from within a model or across external models (or both).";
  }
}

function riskFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "This module (commonly known as Monte Carlo), allows you to risk assess various inputs from the model by producing a distribution of possible outcomes within the Epi+ model(s) created or non-Epi+ created models. It provides the key information required when performing Monte Carlo, for example the percentile graphs over time and Tornado charts.";
  }
}

function demandFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "In this section, the user is able to take a specific series in their forecast and review this from a more operational, demand-based perspective. They can adjust for seasonality, stocking and parallel trade whilst also splitting the sales into different SKUs and comparing the forecast to targets or actual, realized sales.";
  }
}

function dataFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "In this section, the user is able to phase any data they may have down to a more granular level. This could be potentially useful when the data is not in the same frequency as the forecast model (i.e. the data is annual, but the forecast is quarterly). Although this is one of last features of Sales+, this may be useful at the very beginning of the forecast as the user aligns their data with the forecast frequency before inputting their historical values.";
  }
}

function exportFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "This functionality provides the user with the ability to share any FC+ created forecast model with anyone (regardless of whether or not they have an FC+ license installed on their computer). When this feature is selected, the forecast model will be exported to a new (unsaved) Excel file. The resulting workbook will retain the Excel based formula's but all of the forecasting formula's will no longer be operational. This function is useful when the user needs to send the model to a colleague who does not have an FC+ license or when the user wants to share it with others who do have an FC+ license but doesn't want any changes being made to the model.";
  }
}

function extractFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "The user can extract the relevant conversion and eventing datacubes from the forecast model and then import these into a pre-built Microsoft Power BI report for instant analysis. The analytical capabilities in Power BI far exceed those in Excel and using this feature also helps to keep the file size down for your forecast model file. Also, the visualizations are far more attractive in Power BI than Excel, helping to gain buy in and attention to the time you have spent creating your forecast models. Please note that the extract to PBI function is an optional extra that can be included within your license on request.";
  }
}

function deleteFunction() {
  var x = document.getElementsByClassName("text-change");
  if (x[0].className.includes("text-change")) {
    x[0].innerHTML =
      "The user is not able to manually delete sections of created modules. This will compromise the model calculations and render the model inoperable. This occurs because each section is linked by different modules. The Delete Module allows the user to quickly delete any section created without impacting the calculations. Delete any Epi/Trending/SimScore/Eventing/Conversion modules within the excel file, across any sheet.";
  }
}
